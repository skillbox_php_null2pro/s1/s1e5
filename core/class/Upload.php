<?php

namespace Main;

use Main\File;
use Main\FileValidator;

class Upload
{
    const UPLOAD_PATH = '/upload';
    private static $instance;
    private static $fileList;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Upload();
        }

        return self::$instance;
    }

    public static function getFileList()
    {
        if (!self::$fileList) {
            self::updateDir();
        }

        return self::$fileList;
    }

    public static function updateDir()
    {
        $fullUploadPath = $_SERVER['DOCUMENT_ROOT'] . self::UPLOAD_PATH;

        $arFiles = scandir($fullUploadPath, SCANDIR_SORT_NONE);
        foreach ($arFiles as &$file) {
            $file = new File($file);
        }

        self::$fileList = FileValidator::arrayValidate($arFiles);

        return self::$fileList;
    }

    public static function commitNewFilesToUpload()
    {
    }
}
