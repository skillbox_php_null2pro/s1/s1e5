<?php


namespace Main\Cache;


class Cache
{
    const CACHE_PATH = '/cache';
    const CACHE_PATH_FULL = $_SERVER['DOCUMENT_ROOT'] . self::CACHE_PATH;
    private static $instance;

    private function __construct()
    {
    }

    public function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Cache();
        }

        return self::$instance;
    }

    public function isCache($fileName)
    {
        return file_exists(self::CACHE_PATH_FULL . '/' . md5(basename($fileName)));
    }

    public function getFileName($fileName)
    {
        return ($this->isCache($fileName)) ? md5(basename($fileName)) : false;
    }

    public function getFileSrc($fileName)
    {
        $name = $this->getFileName($fileName);
        if ($name) {
            return self::CACHE_PATH . '/' . $name;
        }

        return false;
    }

    public function getFilePath($fileName)
    {
        $name = $this->getFileName($fileName);
        if ($name) {
            return self::CACHE_PATH_FULL . '/' . $name;
        }

        return false;
    }

    public function putToCache($fileName)
    {
        return copy($fileName, self::CACHE_PATH_FULL . '/' . md5(basename($fileName)));
    }
}