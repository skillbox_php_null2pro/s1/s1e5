<?php


namespace Main;


class Config
{
    const CACHE_PATH = '/cache';
    const UPLOAD_PATH = '/upload';
    private static $instance;


    private function __construct()
    {
        $GLOBALS['ROOT'] = dirname(__FILE__, 2);
    }

    public function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

}