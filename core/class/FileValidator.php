<?php

namespace Main\File;

class FileValidator
{
    const IGNORED_NAMES = ['.', '..', 'index.php'];
    const FILE_TYPES = ['image/jpeg', 'image/png'];
    const MAX_FILESIZE = 5 * 1024 * 1024;

    /**Возвращает массив файлов, прошедших фильтры
     *
     * @param array $arFiles    проверяемый массив файлов
     * @param array $arBadFiles отдает массив файлов, не прошедших фильтры
     *
     * @return array очищенный массив файлов
     */
    public static function arrayValidate(array $arFiles, &$arBadFiles = [])
    : array {

        /** @var \Main\File\File $file */
        foreach ($arFiles as $id => $file) {

            if (array_search($file->name, self::IGNORED_NAMES)) {
                unset($arFiles[$id]);
                continue;
            }

            if (!self::fileValidate($file)) {
                $arBadFiles[] = $file;
                unset($arFiles[$id]);
            };
        }

        return $arFiles;
    }

    /**Возвращает результат валидации файла
     *
     * @param \Main\File\File $file - файл-обьект
     *
     * @return bool - результат валидации
     */
    public static function fileValidate(File $file)
    : bool {
        $isValidType = array_search($file->type, self::FILE_TYPES) !== false;
        $isValidSize = $file->size <= self::MAX_FILESIZE;

        return ($isValidType && $isValidSize);
    }
}
