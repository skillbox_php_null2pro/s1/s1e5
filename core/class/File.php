<?php

namespace Main;

class File
{
    public $name;
    public $src;
    public $type;
    public $size;
    private static $finfo;
    private static $objCount;

    function __construct($fileName)
    {
        if (!is_string($fileName)) {
            return false;
        }

        if (!self::$finfo) {
            self::$finfo = finfo_open(FILEINFO_MIME_TYPE);
        }

        self::$objCount++;

        $this->name = $fileName;
        $this->src = Main::$Config['UPLOAD'] . '/' . $fileName;
        $fullPath = $_SERVER['DOCUMENT_ROOT'] . $this->src;
        $this->type = finfo_file(self::$finfo, $fullPath);
        $this->size = filesize($fullPath);
    }

    function __destruct()
    {
        if (!self::$objCount) {
            finfo_close(self::$finfo);
        }
    }
}