<?php

class Main
{
    public static $Config = [];
    private static $instance;

    private function __construct()
    {
        require_once '../.config.php';
        self::$Config = $CONFIG;
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Main();
        }

        return self::$instance;
    }


}