<? require 'include/controller.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv = "Content-Type" content = "text/html; charset=UTF-8"/>
    <link href = "styles.css" rel = "stylesheet"/>
    <script src = "scripts/jquery-3.3.1.js"></script>
    <script src = "scripts/jquery.form.js"></script>
    <script src = "scripts/script.js"></script>
    <title>Фотоальбом</title>
    <style type = "text/css">

    </style>
</head>

<body>

<form name = "upload" method = "post" enctype = "multipart/form-data">
    <input id = "input" type = "file" name = "files[]" multiple>
    <input type = "submit" name = "submit">
</form>
<div id="file_manager"></div>
</body>
</html>
