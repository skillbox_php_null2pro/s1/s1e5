<? require realpath('./..') . '/include/template/header.php';

$Upload = \Main\Upload::getInstance();
$arFiles = $Upload::getFileList();

if (empty($arFiles)) {
    return;
}

?>
<? /** @var \Main\File $File */
foreach ($arFiles as $if => $File): ?>
    <table>
        <tr>
            <td><img src = "<?= $File->src ?>"></td>
        </tr>
        <tr>
            <td><?= $File->name ?></td>
        </tr>
        <tr>
            <td><input type = "checkbox"><label>Удалить</label></td>
        </tr>
    </table>
<? endforeach; ?>